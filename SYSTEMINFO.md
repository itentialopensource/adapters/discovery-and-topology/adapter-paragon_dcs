# Paragon DCS

Vendor: Juniper Networks
Homepage: https://www.juniper.net/

Product: Paragon DCS
Product Page: https://www.juniper.net/us/en/products/network-automation/paragon-automation.html

## Introduction
We classify Paragon DCS into the Discovery & Topology domain as Paragon DCS provides capabilities to enable the discovery and management of network devices and their configuration.

## Why Integrate
The Paragon DCS adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper Paragon DCS. With this adapter you have the ability to perform operations such as:

- Connectivity Service

## Additional Product Documentation
The [API documents for Paragon DCS]()
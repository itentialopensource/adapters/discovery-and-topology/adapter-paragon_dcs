
## 0.2.9 [10-14-2024]

* Changes made at 2024.10.14_18:40PM

See merge request itentialopensource/adapters/adapter-paragon_dcs!12

---

## 0.2.8 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-paragon_dcs!10

---

## 0.2.7 [08-14-2024]

* Changes made at 2024.08.14_17:21PM

See merge request itentialopensource/adapters/adapter-paragon_dcs!9

---

## 0.2.6 [08-07-2024]

* Changes made at 2024.08.06_18:20PM

See merge request itentialopensource/adapters/adapter-paragon_dcs!8

---

## 0.2.5 [03-25-2024]

* Changes made at 2024.03.25_11:04AM

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dcs!6

---

## 0.2.4 [03-13-2024]

* Changes made at 2024.03.13_12:45PM

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dcs!5

---

## 0.2.3 [03-11-2024]

* Update Metadata

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dcs!4

---

## 0.2.2 [02-26-2024]

* Changes made at 2024.02.26_10:58AM

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dcs!3

---

## 0.2.1 [12-29-2023]

* fix metadata

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dcs!2

---

## 0.2.0 [12-29-2023]

* More migration changes

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dcs!1

---

## 0.1.1 [10-03-2023]

* Bug fixes and performance improvements

See commit 718090b

---
